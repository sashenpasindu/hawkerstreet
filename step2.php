<?php include 'inc/header.php'; ?>
<div class="main-container container-fluid">
    <div class="row">

        <div class="col-md-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="step-bar">
                        <div class="step done">
                            <i class="fal fa-check"></i>
                        </div>
                        <div class="step-line done"></div>
                        <div class="step active">
                            <i class="fas fa-circle"></i>
                        </div>
                        <div class="step-line"></div>
                        <div class="step"> </div>
                        <div class="step-line"></div>
                        <div class="step"> </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-uppercase m-t-b-md text-center">Your personal details</h3>
                </div>
                <div class="col-sm-12">
                    <form>
                        <div class="form-group">
                            <label>Title</label>
                            <select class="form-control">
                                <option>Mr.</option>
                                <option>Mrs.</option>
                                <option>Miss.</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>NIC/Passport/Driving License</label>
                            <input type="text" class="form-control" placeholder="NIC/Passport/Driving License">
                        </div>
                        <div class="form-group">
                            <label>Mobile</label>
                            <input type="text" class="form-control" placeholder="Eg: 0777123456">
                        </div>
                        <div class="form-group">
                            <label>Telephone</label>
                            <input type="text" class="form-control" placeholder="Eg: 0112123456">
                        </div> 
                        <div class="form-group m-t-b-md">
                            <a class="btn btn-primary btn-lg center-block-xs" href="step3.php">Next</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <div class="col-md-7">
            <div class="">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner" role="listbox">
                        <div class="item ">
                            <img src="img/carousel/1.jpg" alt="..."> 
                        </div>
                        <div class="item active">
                            <img src="img/carousel/2.jpg" alt="..."> 
                        </div> 
                        <div class="item">
                            <img src="img/carousel/3.jpg" alt="..."> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<!-- /#page-content-wrapper -->

</div>

<?php
include 'inc/footer.php';
