<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Hawker Street</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.0.13/css/all.css" crossorigin="anonymous">

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]--> 
        <link href="css/checkbox.css" rel="stylesheet" type="text/css"/>
        <link href="css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="css/main.css" rel="stylesheet" type="text/css"/>
        <link href="css/margins.css" rel="stylesheet" type="text/css"/>
        <link href="css/responsive.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="wrapper" >

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand">
                        <form class="navbar-form navbar-left">
                            <i class="far fa-search"></i>
                            <input type="text" class="form-control" placeholder="Search anything...">
                        </form>
                    </li> 
                    <li>
                        <a href="#">Sign In</a>
                    </li>
                    <li>
                        <a href="#">Register</a>
                    </li>
                </ul>
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">
                <nav class="navbar navbar-default">
                    <div class="container-fluid main-container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <a href="#" class="menu-toggle menu-toggle-btn visible-xs"><div class="menu icon"></div></a>
                            <a class="navbar-brand" href="#">

                                <img src="img/logo.png" alt="HawkerStreet" class="img-responsive ">
                                <!--<img src="img/logo-xs.png" alt="HawkerStreet" class="img-responsive visible-xs">-->
                            </a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="left-menu">
                            <form class="navbar-form navbar-left">
                                <i class="far fa-search"></i>
                                <input type="text" class="form-control" placeholder="Search anything...">
                            </form>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Sign In</a></li>   
                                <form class="navbar-form navbar-left">
                                    <button type="submit" class="btn btn-primary">Register</button>
                                </form>
                                <li><a href="#" ><i class="far fa-shopping-cart"></i></a></li>   
                            </ul>
                            <div class="clearfix"></div>

                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>