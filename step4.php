<?php include 'inc/header.php'; ?>
<div class="main-container container-fluid">
    <div class="row">

        <div class="col-md-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="step-bar">
                        <div class="step done">
                            <i class="fal fa-check"></i>
                        </div>
                        <div class="step-line done"></div>
                        <div class="step done">
                            <i class="fal fa-check"></i>
                        </div>
                        <div class="step-line done"></div>
                        <div class="step done">
                            <i class="fal fa-check"></i>
                        </div>
                        <div class="step-line done"></div>
                        <div class="step active">
                            <i class="fas fa-circle"></i>
                        </div> 
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-uppercase m-t-b-md text-center">And finalyy!</h3>
                </div>
                <div class="col-sm-12">
                    <form>
                        
                        <div class="form-group">
                            <label>Bank account number</label>
                            <input type="text" class="form-control" placeholder="Bank account number">
                        </div>
                        <div class="form-group">
                            <label>Bank account Name</label>
                            <input type="text" class="form-control" placeholder="Account Name">
                        </div>
                        <div class="form-group">
                            <label>Bank Name</label>
                            <input type="text" class="form-control" placeholder="Bank Name">
                        </div> 
                        <div class="form-group">
                            <label>Bank branch</label>
                            <input type="text" class="form-control" placeholder="Bank Branch">
                        </div> 
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control">
                                <option>Select Status...</option> 
                            </select>
                        </div>
                        <div class="form-group m-t-b-md">
                            <a class="btn btn-primary btn-lg center-block-xs" href="#">Done</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <div class="col-md-7">
            <div class="">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner" role="listbox">
                        <div class="item ">
                            <img src="img/carousel/1.jpg" alt="..."> 
                        </div>
                        <div class="item ">
                            <img src="img/carousel/2.jpg" alt="..."> 
                        </div> 
                        <div class="item active">
                            <img src="img/carousel/3.jpg" alt="..."> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<!-- /#page-content-wrapper -->

</div>

<?php
include 'inc/footer.php';
