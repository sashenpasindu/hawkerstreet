<?php include 'inc/header.php'; ?>
<div class="no-padding-container container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="main-image-or-carousel">
                <div id="carousel-example-generic" class="carousel cropped slide" data-ride="carousel">

                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="img/carousel/1.jpg" alt="..."> 
                        </div>
                        <div class="item">
                            <img src="img/carousel/2.jpg" alt="..."> 
                        </div> 
                        <div class="item">
                            <img src="img/carousel/3.jpg" alt="..."> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-uppercase m-t-b-md">Become a vendor</h3>
                </div>
                <div class="col-sm-6">
                    <form>
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" placeholder="Last Name">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control" placeholder="Confirm Password">
                        </div>
                        <div class="form-group m-t-b-sm">
                            <label class="label">
                                <input  class="label__checkbox" type="checkbox" name="her_birthday"/> 
                                <span class="label__text pull-left">
                                    <span class="label__check">
                                        <i class="fal fa-check icon-tick"></i>
                                    </span>
                                </span>
                                <span class="lable-text pull-left">I accept Terms & Conditions</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <a href="step2.php" class="btn btn-primary btn-lg center-block-xs">Register</a>
                        </div>
                    </form>
                </div>
                <div class="col-sm-2 ">
                    <div class="or-line hidden-xs"></div>
                    <div class="or hidden-xs">OR</div>
                    <div class="or-xs text-center m-t-b-sm visible-xs">OR</div>
                </div>
                <div class="col-sm-4 col-xs-8 col-xs-offset-2 col-sm-offset-0">
                    <div class="social-logins">
                        <p class="text-center">Register with</p>
                        <a class="btn btn-info facebook btn-block"><i class="fab fa-facebook-f"></i> Facebook</a>
                        <a class="btn btn-info google btn-block"><i class="fab fa-google"></i> Google</a>
                        <a class="btn btn-info twitter btn-block"><i class="fab fa-twitter"></i> Twitter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<!-- /#page-content-wrapper -->

</div>

<?php
include 'inc/footer.php';
