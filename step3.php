<?php include 'inc/header.php'; ?>
<div class="main-container container-fluid">
    <div class="row">

        <div class="col-md-5">
            <div class="row">
                <div class="col-lg-12">
                    <div class="step-bar">
                        <div class="step done">
                            <i class="fal fa-check"></i>
                        </div>
                        <div class="step-line done"></div>
                        <div class="step done">
                            <i class="fal fa-check"></i>
                        </div>
                        <div class="step-line done"></div>
                        <div class="step active">
                            <i class="fas fa-circle"></i>
                        </div>
                        <div class="step-line"></div>
                        <div class="step"> </div> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="text-uppercase m-t-b-md text-center">You’re Almost there</h3>
                </div>
                <div class="col-sm-12">
                    <form>

                        <div class="form-group">
                            <label>Company Name</label>
                            <input type="text" class="form-control" placeholder="Company Name">
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            <select class="form-control">
                                <option>Food</option>
                                <option>Jewelry</option>
                                <option>Home & Garden</option>
                                <option>Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Vendor Description</label>
                            <textarea rows="4" class="form-control" placeholder="Eg: Lorem ipsum dolor sit amet, consectetnon orci. Integer tempus varius sem nec varius.  Vestibulum velit orci, iaculis eget malesuada ut, gravida quis ligula."></textarea>
                        </div>
                        <div class="form-group">
                            <label>Business Registration <small class="text-capitalize">(Optional)</small></label>
                            <input type="text" class="form-control" placeholder="Business Registration">
                        </div> 
                        <div class="form-group">
                            <label>Pickup Address</label>
                            <input type="text" class="form-control" placeholder="Pickup Address">
                        </div> 
                        <div class="form-group m-t-b-md">
                            <a class="btn btn-primary btn-lg center-block-xs" href="step4.php">Next</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <div class="col-md-7">
            <div class="">
                <iframe width="100%" height="680" frameborder="0" style="border:0;     margin-bottom: -5px;" src="https://www.google.com/maps/embed/v1/view?zoom=12&center=6.9271%2C79.8612&key=AIzaSyCwu5IG_x_0TekUbn5Vpq0gwwWdSAwKkTI" allowfullscreen></iframe>
            </div>
        </div>
    </div>

</div>
</div>
<!-- /#page-content-wrapper -->

</div> 
<?php
include 'inc/footer.php';
